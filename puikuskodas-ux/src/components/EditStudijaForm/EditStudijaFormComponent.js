import React from "react";

const EditStudijaFormComponent = ({
  handlePavadinimasChange,
  handleLogotipasChange,
  handleDydisChange,  
  handleKategorijaChange,
  handleSubmit,
  handleCancel,
  pavadinimas,
  logotipas,
  dydis,
  kategorija,
}) => {
  return (
    <form className="container">
      <div className="form-group">
        <label htmlFor="pavadinimas">Pavadinimas</label>
        <input
          type="text"
          className="form-control"
          id="pavadinimas"
          placeholder="Enter pavadinimas"
          onChange={handlePavadinimasChange}
          value={pavadinimas}
        />
      </div>
      <div className="form-group">
        <label htmlFor="logotipas">Logotipas</label>
        <input
          type="text"
          className="form-control"
          id="logotipas"
          placeholder="Enter logotipas"
          onChange={handleLogotipasChange}
          value={logotipas}
        />
      </div>      
      
      <div className="form-group">
        <label htmlFor="dydis">Dydis</label>
        <select
          className="form-control"
          id="dydis"
          placeholder="Enter dydis"
          onChange={handleDydisChange}
          value={dydis}
        >
        <option value="" defaultValue disabled hidden>
            Choose Dydis
          </option>          
          <option value="maza">Maza</option>
          <option value="vidutine">Vidutine</option>
          <option value="didele">Didele</option>          
        </select>      
      </div>

      <div className="form-group">
        <label htmlFor="kategorija">Select kategorija</label>
        <select
          className="form-control"
          id="kategorija"
          onChange={handleKategorijaChange}
          value={kategorija}
        >
          <option value="" defaultValue disabled hidden>
            Choose Kategorija
          </option>          
          <option value="Valstybine">Valstybine</option>
          <option value="Namu">Namu</option>
          <option value="Gyvai">Gyvai (live)</option>          
        </select>
      </div>


      <button
        type="submit"
        onClick={handleSubmit}
        className="btn btn-success mr-2"
      >
        Save
      </button>
      <button onClick={handleCancel} className="btn btn-secondary">
        Cancel
      </button>
    </form>
  );
};

export default EditStudijaFormComponent;