import React from "react";
//import food from "../../Images/food.jpg";


class StudijaAdministrationComponent extends React.Component {
  render() {
    return (
      <tr>
        <th scope="row">{this.props.rowNr}</th>
        <td>
          <img
            //src={food}
            className="card-img-top img-size-for-table"
            alt="food"
          />
        </td>
        <td>{this.props.pavadinimas}</td>
        <td>{this.props.aprasymas}</td>
        <td>
          <button
            className="btn btn-warning mr-2"
            onClick={this.props.handleEditClick}
          >
            Edit
          </button>
          <button
            className="btn btn-danger mr-2"
            onClick={this.props.handleDeleteClick}
          >
            Delete
          </button>

        </td>
      </tr>
    );
  }
}

export default StudijaAdministrationComponent;