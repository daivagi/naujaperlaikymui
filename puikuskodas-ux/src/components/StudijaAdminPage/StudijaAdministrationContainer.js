import React from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";
import StudijaAdministrationComponent from "./StudijaAdministrationComponent";
import AddNewStudijaComponent from "./AddNewStudijaComponent";

class StudijaAdministrationContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      studijos: []
    };
  }

  getAdministrationStudijos = () => {
    axios
      .get("http://localhost:8081/PuikusKodas/api/studijos")
      .then(response => {
        this.setState({ studijos: response.data });
      })
      .catch(error => {
        console.log(error);
      });
  };
  componentDidMount() {
    this.getAdministrationStudijos();
  }

  handleAddNewStudijaClick = event => {
    event.preventDefault();

    this.props.history.push("/admin/new-studija");
  };

  handleEditClick = (event, id) => {
    event.preventDefault();
    this.props.history.push("/admin/edit-studija/" + id);
  };

  handleDeleteClick = (event, id) => {
    event.preventDefault();
    axios
      .delete("http://localhost:8081/PuikusKodas/api/studijos/" + id)
      .then(() => {
        this.getAdministrationStudijos();
      })
      .catch(error => {
        console.log(error);
      });
  };



  render() {
    const studijaItems = this.state.studijos.map((studija, index) => (
      <StudijaAdministrationComponent
        key={index}
        rowNr={index + 1}
        pavadinimas={studija.pavadinimas}
        aprasymas={studija.aprasymas}
        id={studija.id}
        handleEditClick={event => this.handleEditClick(event, studija.id)}
        handleDeleteClick={event => this.handleDeleteClick(event, studija.id)}
      />
    ));

    return (
      <div className="container">
        <AddNewStudijaComponent
          handleAddNewstudija
    Click={this.handleAddNewStudijaClick}
        />
        <table className="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Pavadinimas</th>
              <th scope="col">Aprasymas</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>{studijaItems}</tbody>
        </table>
      </div>
    );
  }
}

export default withRouter(StudijaAdministrationContainer);