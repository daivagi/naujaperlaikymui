import React from "react";

const AddNewStudijaComponent = ({ handleAddNewStudijaClick }) => {
  return (
    <button className="btn btn-secondary" onClick={handleAddNewStudijaClick}>
      Prideti studija
    </button>
  );
};

export default AddNewStudijaComponent;