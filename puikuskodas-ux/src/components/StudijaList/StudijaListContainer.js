import React from "react";
import axios from "axios";
import StudijaListComponent from "./StudijaListComponent";
import AddNewStudijaComponent from "../StudijaAdminPage/AddNewStudijaComponent";
//import Diagrama from "../DoughnutExample/Diagrama";

class StudijaListContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      studijos: [],
      studijaLength: 0
    };
  }
  componentDidMount() {
    axios
      .get("http://localhost:8081/PuikusKodas/api/studijos")
      .then(response => {
        this.setState({
          studijos: response.data,
          studijaLength: response.data.length //alert
        });

        this.dovanaAlert(); //alert
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleAddNewStudijaClick = event => {
    event.preventDefault();

    this.props.history.push("/admin/new-studija");
  };

  //alert
  studijaAlert = () => {
    if (this.state.studijaLength > 1) {
      document
        .getElementById("alertstudijos")
        .setAttribute("class", "d-none");
    } else {
      document
        .getElementById("alertstudijos")
        .setAttribute("class", "alert alert-danger");
    }
  };

  render() {
    const studijaItems = this.state.studijos.map(
      (studija, index) => (
        <StudijaListComponent
          key={index}
          pavadinimas={studija.pavadinimas}
          kaina={studija.kaina}
          paveiksliukas={studija.paveiksliukas}
          kategorija={studija.kategorija}
          aprasymas={studija.aprasymas} 
          
        //  onDetailsClick={event => this.onDetailsClick(event, product.id)}
        />
      )
    );

    return (
      <div className="container">
        <div class="row justify-content-center">
        {/* alert */}
        <div id="alertstudijos" className="alert alert-info py-4 px-5" role="alert">
          
          Studiju yra mažiau nei 1.
          
          <p className="pt-4 fluid"><AddNewStudijaComponent
          handleAddNewStudijaClick={this.handleAddNewStudijaClick}
        />
        </p>
          
        </div>
        {/* alert */}
        <div className="card-deck">{studijaItems}</div>
         {/* <Diagrama /> */}
      </div>  
      </div>    
    );
  }
}

export default StudijaListContainer;