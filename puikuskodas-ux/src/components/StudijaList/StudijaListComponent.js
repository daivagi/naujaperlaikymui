import React from "react";


class StudijaCartComponent extends React.Component {
  render() {
    return (
      <div className="col-sm-6 col-md-4">
        <div className="card" style={{ width: "18rem" }}>
          {/* <img src={food} className="card-img-top img-size" alt="food" /> */}
          <div className="card-body">
            <h5 className="card-title">{this.props.pavadinimas}</h5>
            <p className="card-text">{this.props.kaina}€</p>
            <p className="card-text">{this.props.kategorija}</p>
            <p className="card-text">{this.props.aprasymas}</p>            
            {/* <button
              className="btn btn-primary"
              onClick={this.props.onDetailsClick}
            >
              Details
            </button> */}
          </div>
        </div>
      </div>
    );
  }
}

export default StudijaCartComponent;