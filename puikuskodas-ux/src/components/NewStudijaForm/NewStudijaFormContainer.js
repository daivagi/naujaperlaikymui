import React from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";
import NewStudijaFormComponent from "./NewStudijaFormComponent";

class NewStudijaFormContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      studija: {
        pavadinimas: "",
        kaina: "",
        aprasymas: "",
        paveiksliukas: "",
        kategorija: ""        
      }
    };
  }

  handlePavadinimasChange = event => {
    var pavadinimasValue = event.target.value;
    this.setState(prevState => {
      let studija = Object.assign({}, prevState.studija);
      studija.pavadinimas = pavadinimasValue;
      return { studija };
    });
  };

  handleKainaChange = event => {
    var kainaValue = event.target.value;
    this.setState(prevState => {
      let studija = Object.assign({}, prevState.studija);
      studija.kaina = kainaValue;
      return { studija };
    });
  };

  handleAprasymasChange = event => {
    var aprasymasValue = event.target.value;
    this.setState(prevState => {
      let studija = Object.assign({}, prevState.studija);
      studija.aprasymas = aprasymasValue;
      return { studija };
    });
  };
  handlePaveiksliukasChange = event => {
    var paveiksliukasValue = event.target.value;
    this.setState(prevState => {
      let studija = Object.assign({}, prevState.studija);
      studija.paveiksliukas = paveiksliukasValue;
      return { studija };
    });
  };

  handleKategorijaChange = event => {
    var kategorijaValue = event.target.value;
    this.setState(prevState => {
      let studija = Object.assign({}, prevState.studija);
      studija.kategorija = kategorijaValue;
      return { studija };
    });
  };



  handleCancel = event => {
    event.preventDefault();
    this.props.history.push("/studijos");
  };

  handleSubmit = event => {
    event.preventDefault();
    axios     
      .post("http://localhost:8081/PuikusKodas/api/studijos/", {
        pavadinimas: this.state.studija.pavadinimas,
        kaina: this.state.studija.kaina,
        aprasymas: this.state.studija.aprasymas,
        paveiksliukas: this.state.studija.paveiksliukas,
        kategorija: this.state.studija.kategorija,

      })
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });

    this.setState({
      studija: {
        pavadinimas: "",
        kaina: "",
        aprasymas: "",
        paveiksliukas: "",
        kategorija: ""
      }
    });
  };

  render() {
    return (
      <NewStudijaFormComponent
        handlePavadinimasChange={this.handlePavadinimasChange}
        handleKainaChange={this.handleKainaChange}
        handleAprasymasChange={this.handleAprasymasChange}
        handlePaveiksliukasChange={this.handlePaveiksliukasChange}
        handleKategorijaChange={this.handleKategorijaChange}
        handleSubmit={this.handleSubmit}
        handleCancel={this.handleCancel}
        pavadinimas={this.state.studija.pavadinimas}
        kaina={this.state.studija.kaina}
        aprasymas={this.state.studija.aprasymas}
        paveiksliukas={this.state.studija.paveiksliukas}
        kategorija={this.state.studija.kategorija}
      />
    );
  }
}

export default withRouter(NewStudijaFormContainer);