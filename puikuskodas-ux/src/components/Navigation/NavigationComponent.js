import React from "react";
import { Link } from "react-router-dom";
import first from "../../Images/first.jpg"
 
class NavigationComponent extends React.Component {

  render() {
    return (
      <div className="container mb-3">
        <img src={first} alt="first"></img>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="collapse navbar-collapse">
            <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
              <li className="nav-item">
                <Link className="nav-link" to="/">
                  Home
                </Link>
              </li>

              <li className="nav-item">
                <Link className="nav-link" to="/studijos">
                  Studijos
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/admin/new-studija">
                  Ivesti Studija
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/new-atlikejai">
                  Atlikejai
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/admin/studijos">
                  Studiju administravimas
                </Link>
              </li>              
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default NavigationComponent;