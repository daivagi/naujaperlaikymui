import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { Switch, Route } from "react-router";
import { BrowserRouter } from "react-router-dom";
import NavigationComponent from "./components/Navigation/NavigationComponent";
//import NewLaiskasFormContainer from "./components/NewLaiskasForm/NewLaiskasFormContainer";
import StudijaListContainer from "./components/StudijaList/StudijaListContainer";
import NewStudijaFormContainer from "./components/NewStudijaForm/NewStudijaFormContainer";
import EditStudijaFormContainer from "./components/EditStudijaForm/EditStudijaFormContainer";
import StudijaAdministrationContainer from "./components/StudijaAdminPage/StudijaAdministrationContainer";
var AppContainer = props => {
  return (
    <div>
      <NavigationComponent />
      {props.children}
    </div>
  );
};

var NoMatch = props => {
  var goApp = () => props.history.push("/");
  return (
    <div>
      Sorry, this route does not exists
      <button onClick={goApp}>Go Home</button>
    </div>
  );
};


ReactDOM.render(
  <BrowserRouter>    
      <AppContainer>
        <Switch>
          {/* <Route path="/new-laiskas" component={NewLaiskasFormContainer} /> */}
          <Route exact path="/" component={StudijaListContainer} />
          <Route path="/studijos" component={StudijaListContainer} />
          <Route path="/admin/studijos" component={StudijaAdministrationContainer} />
          <Route
            exact
            path="/admin/new-studija"
            component={NewStudijaFormContainer}
          />

          <Route
            path="/admin/edit-studija/:id"
            component={EditStudijaFormContainer}
          />



          <Route path="*" component={NoMatch} />
          <Route component={NoMatch} />
        </Switch>
      </AppContainer>    
  </BrowserRouter>,

  document.getElementById("root")
);

serviceWorker.unregister();