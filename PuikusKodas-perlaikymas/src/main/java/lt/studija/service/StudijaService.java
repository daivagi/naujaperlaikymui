package lt.studija.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lt.studija.dao.StudijaRepository;
import lt.studija.model.studija.NaujaStudija;
import lt.studija.model.studija.Studija;




@Service
public class StudijaService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StudijaService.class);


	private StudijaRepository studijaRepository;

	@Autowired
	public StudijaService(StudijaRepository studijaRepository) {
		this.studijaRepository = studijaRepository;
	}

	@Transactional
	public List<Studija> getStudijos() {
		return  studijaRepository.findAllByOrderByPavadinimasAsc().stream()
				.map((studija) -> new Studija(studija.getId(), studija.getPavadinimas(), studija.getDydis(),
						studija.getLogotipas(), studija.getKategorija()))
				.collect(Collectors.toList());
	}

	@Transactional
	public Studija getStudija(Long id) {
		return studijaRepository.findAll().stream().filter(studija -> studija.getId().equals(id)).findFirst()
				.orElseThrow(() -> new RuntimeException("Can't find"));
	}

	@Transactional
	public void createStudija(NaujaStudija naujaStudija) {
		Studija studija = new Studija();		
		studija.setDydis(naujaStudija.getDydis());
		studija.setPavadinimas(naujaStudija.getPavadinimas());
		studija.setLogotipas(naujaStudija.getLogotipas());
		studija.setKategorija(naujaStudija.getKategorija());
		//studijaRepository.save(studija);
	}

	@Transactional
	public Studija updateStudija(Long id, NaujaStudija naujaStudija) {
		Studija existingStudija = getStudija(id);		
		existingStudija.setDydis(naujaStudija.getDydis());
		existingStudija.setPavadinimas(naujaStudija.getPavadinimas());
		existingStudija.setLogotipas(naujaStudija.getLogotipas());
		existingStudija.setKategorija(naujaStudija.getKategorija());
		return existingStudija;
	}

	@Transactional
	public void deleteStudija(Long id) {
		studijaRepository.deleteById(id);

	}
	
	@PostConstruct
	public void init() {
		LOGGER.info("Service bean is created. Classname: " + getClass().toString() + " . Scope: singelton");

	}

	@PreDestroy
	public void destroy() {
		LOGGER.info("Service bean is destroyed. Classname: " + getClass().toString()+ " . Scope: singelton");
	}
}
