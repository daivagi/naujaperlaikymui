package lt.studija.service;
//package lt.studija.service;
//
//import java.util.List;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import lt.studija.dao.TeikejasRepository;
//import lt.studija.model.atlikejas.TeikejasKlientui;
//
//
//@Service
//public class AtlikejasService {
//
//	private static final Logger LOGGER = LoggerFactory.getLogger(TeikejasService.class);
//
//	private TeikejasRepository teikejasRepository;
//
//	@Autowired
//	public TeikejasService(TeikejasRepository teikejasRepository) {
//		this.teikejasRepository = teikejasRepository;
//	}
//
//	@Transactional(readOnly = true)
//	public List<TeikejasKlientui> getLaiskai() {
//		return teikejasRepository.findAllByOrderByPavadinimas().stream().map(
//				(teikejas) -> new TeikejasKlientui(laiskas.getPavadinimas(), laiskas.getVardas(), laiskas.getPavarde(), laiskas.getAdresas()))
//				.collect(Collectors.toList());
//	}
//
//	@Transactional
//	public LaiskasKlientui getLaiskasKlientui(String pavadinimas) {
//		Laiskas laiskas = getLaiskas(pavadinimas);
//		ApplicationContext context = new ClassPathXmlApplicationContext(
//				"application-context.xml");
//		LaiskasKlientui laiskasKlientui = (LaiskasKlientui) context.getBean("laiskasKlientuiBean");
//		laiskasKlientui.setPavadinimas(laiskas.getPavadinimas());
//		laiskasKlientui.setVardas(laiskas.getVardas());
//		laiskasKlientui.setPavarde(laiskas.getPavarde());
//		laiskasKlientui.setAdresas(laiskas.getAdresas());
//		((ConfigurableApplicationContext) context).close();
//		return laiskasKlientui;	
//				
//	}
//
//	@Transactional
//	public Laiskas getLaiskas(String pavadinimas) {
//		return teikejasRepository.findAll().stream().filter(l -> l.getPavadinimas().equals(pavadinimas)).findFirst()
//				.orElseThrow(() -> new RuntimeException("Can't find laiskas"));
//	}
//	 
//	
//	
//	@Transactional
//	public void createLaiskas(NaujasLaiskas naujasLaiskas) {
//		Laiskas laiskas = new Laiskas();
//		laiskas.sukurtiPavadinima(naujasLaiskas.getVardas(), naujasLaiskas.getPavarde());
//		laiskas.setVardas(naujasLaiskas.getVardas());
//		laiskas.setPavarde(naujasLaiskas.getPavarde());
//		laiskas.setAdresas(naujasLaiskas.getAdresas());
//		teikejasRepository.save(laiskas);
//	}
//
//	@Transactional
//	public Laiskas updateLaiskas(String pavadinimas, NaujasLaiskas naujasLaiskas) {
//		Laiskas existingLaiskas = getLaiskas(pavadinimas);
//		existingLaiskas.setVardas(naujasLaiskas.getVardas());
//		existingLaiskas.setPavarde(naujasLaiskas.getPavarde());
//		existingLaiskas.setAdresas(naujasLaiskas.getAdresas());
//		return existingLaiskas;
//
//	}
//
//	@Transactional
//	public void deleteLaiskas(String pavadinimas) {
//		teikejasRepository.deleteByPavadinimas(pavadinimas);
//	}
//	
//	@Transactional
//	public Integer surastiLaiskuKieki() {
//		return teikejasRepository.surastiLaiskuKieki();
//	}
//
//	@PostConstruct
//	public void init() {
//		LOGGER.info("Service bean is created. Classname: " + getClass().toString() + " . Scope: singelton");
//
//	}
//
//	@PreDestroy
//	public void destroy() {
//		LOGGER.info("Service bean is destroyed. Classname: " + getClass().toString()+ " . Scope: singelton");
//	}
//
//}
