package lt.studija.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import lt.studija.model.studija.Studija;


public interface StudijaRepository extends JpaRepository<Studija, Long>{
	List<Studija> findAllByOrderByPavadinimasAsc();
}
