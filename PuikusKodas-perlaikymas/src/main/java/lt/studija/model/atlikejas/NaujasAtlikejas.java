package lt.studija.model.atlikejas;
//package lt.studija.model.atlikejas;
//
//import javax.validation.constraints.NotNull;
//
//import org.hibernate.validator.constraints.Length;
//
//public class NaujasTeikejas {
//
//	@NotNull
//	@Length(min = 1, max = 50)
//	private String pavadinimas;
//	@NotNull
//	@Length(min = 1, max = 50)
//	private String miestas;
//	@NotNull
//	@Length(min = 1, max = 10)
//	private Long imonesKodas;
//	@NotNull
//	@Length(min = 1, max = 5)
//	private Integer ivertinimas;
//	
//	
//	public NaujasTeikejas() {}
//
//
//	public NaujasTeikejas(String pavadinimas, String miestas, Long imonesKodas, Integer ivertinimas) {		
//		this.pavadinimas = pavadinimas;
//		this.miestas = miestas;
//		this.imonesKodas = imonesKodas;
//		this.ivertinimas = ivertinimas;
//	}
//
//
//	public String getPavadinimas() {
//		return pavadinimas;
//	}
//
//
//	public void setPavadinimas(String pavadinimas) {
//		this.pavadinimas = pavadinimas;
//	}
//
//
//	public String getMiestas() {
//		return miestas;
//	}
//
//
//	public void setMiestas(String miestas) {
//		this.miestas = miestas;
//	}
//
//
//	public Long getImonesKodas() {
//		return imonesKodas;
//	}
//
//
//	public void setImonesKodas(Long imonesKodas) {
//		this.imonesKodas = imonesKodas;
//	}
//
//
//	public Integer getIvertinimas() {
//		return ivertinimas;
//	}
//
//
//	public void setIvertinimas(Integer ivertinimas) {
//		this.ivertinimas = ivertinimas;
//	}
//	
//	
//}
