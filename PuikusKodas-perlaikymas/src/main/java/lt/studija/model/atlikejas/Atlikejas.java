package lt.studija.model.atlikejas;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class Atlikejas {

	@Id
	private String pavadinimas;	
	private String miestas;
	private Long imonesKodas;
	private Integer ivertinimas;
	private String tipas;
	
	public String getPavadinimas() {
		return pavadinimas;
	}
	public void setPavadinimas(String pavadinimas) {
		this.pavadinimas = pavadinimas;
	}
	public String getMiestas() {
		return miestas;
	}
	public void setMiestas(String miestas) {
		this.miestas = miestas;
	}
	public Long getImonesKodas() {
		return imonesKodas;
	}
	public void setImonesKodas(Long imonesKodas) {
		this.imonesKodas = imonesKodas;
	}
	public Integer getIvertinimas() {
		return ivertinimas;
	}
	public void setIvertinimas(Integer ivertinimas) {
		this.ivertinimas = ivertinimas;
	}
	public String getTipas() {
		return tipas;
	}
	public void setTipas(String tipas) {
		this.tipas = tipas;
	}
	
	
	
//	public String sukurtiPavadinima(String vardas, String pavarde) {
//		return this.pavadinimas = vardas+pavarde;
//	}
	
//	public String getPavadinimas() {
//		return pavadinimas;
//	}

	/*
	 * public void setPavadinimas(String pavadinimas) { this.pavadinimas =
	 * pavadinimas; }
	 */
}
