package lt.studija.model.studija;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Studija {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(unique=true)
	private String pavadinimas;	
	private String logotipas;
	private String dydis;
	private String kategorija;
	
	
	public Studija(Long id, String pavadinimas, String logotipas, String dydis, String kategorija) {
		this.id = id;
		this.pavadinimas = pavadinimas;
		this.logotipas = logotipas;
		this.dydis = dydis;
		this.kategorija = kategorija;
	}

	public Studija() {}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPavadinimas() {
		return pavadinimas;
	}
	public void setPavadinimas(String pavadinimas) {
		this.pavadinimas = pavadinimas;
	}
	public String getLogotipas() {
		return logotipas;
	}
	public void setLogotipas(String logotipas) {
		this.logotipas = logotipas;
	}
	public String getDydis() {
		return dydis;
	}
	public void setDydis(String dydis) {
		this.dydis = dydis;
	}
	public String getKategorija() {
		return kategorija;
	}
	public void setKategorija(String kategorija) {
		this.kategorija = kategorija;
	}	
	
	
}
