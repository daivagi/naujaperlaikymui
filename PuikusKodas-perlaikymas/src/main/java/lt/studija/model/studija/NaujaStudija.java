package lt.studija.model.studija;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class NaujaStudija {
	
	@NotNull
	@Length(min = 1, max = 50)
	private String pavadinimas;
	@NotNull
	@Length(min = 1, max = 50)
	private String logotipas;
	@NotNull
	@Length(min = 1, max = 50)
	private String dydis;
	@NotNull
	@Length(min = 1, max = 50)
	private String kategorija;

	public String getPavadinimas() {
		return pavadinimas;
	}

	public void setPavadinimas(String pavadinimas) {
		this.pavadinimas = pavadinimas;
	}

	public String getLogotipas() {
		return logotipas;
	}

	public void setLogotipas(String logotipas) {
		this.logotipas = logotipas;
	}

	public String getDydis() {
		return dydis;
	}

	public void setDydis(String dydis) {
		this.dydis = dydis;
	}

	public String getKategorija() {
		return kategorija;
	}

	public void setKategorija(String kategorija) {
		this.kategorija = kategorija;
	}


}
