package lt.studija.controller;
//package lt.studija.controller;
//
//@RestController
//@Api(value = "laiskas")
//@RequestMapping(value = "/api/laiskas")
//public class TeikejasController {
//
//	
//	private final LaiskasService laiskasService;
//
//	@Autowired
//	public LaiskasController(LaiskasService laiskasService) {
//		this.laiskasService = laiskasService;
//	}
//
//	
//	@RequestMapping(path = "/lasiku-kiekis",method = RequestMethod.GET)
//	@ApiOperation(value = "Get Laisku kiekis", notes = "Returns laisku kiekis")
//	public Integer getLaiskuKiekis() {
//		return laiskasService.surastiLaiskuKieki();
//	}
//	
//	@RequestMapping(method = RequestMethod.GET)
//	@ApiOperation(value = "Get Laiskai", notes = "Returns list of all laiskai")
//	public List<LaiskasKlientui> getLaiskai() {
//		return laiskasService.getLaiskai();
//	}
//
//	@RequestMapping(path = "/{pavadinimas}", method = RequestMethod.GET)
//	@ApiOperation(value = "Get Laiskas", notes = "Returns laiskas by pavadinimas")
//	public LaiskasKlientui getLaiskasKlientui(@PathVariable String pavadinimas) {
//		return laiskasService.getLaiskasKlientui(pavadinimas);
//	}
//	
//		
//
//	@RequestMapping(method = RequestMethod.POST)
//	@ApiOperation(value = "Create laiskas", notes = "Creates laiskas with data")
//	@ResponseStatus(HttpStatus.CREATED)
//	public void createLaiskas(
//			@ApiParam(value = "Laiskas Data", required = true) @Valid @RequestBody final NaujasLaiskas naujasLaiskas) {
//		laiskasService.createLaiskas(naujasLaiskas);
//
//	}
//
//	@RequestMapping(path = "/{pavadinimas}", method = RequestMethod.PUT)
//	@ApiOperation(value = "Update laiskas", notes = "Update laiskas with data")
//	public Laiskas updateLaiskas(@ApiParam(value = "Book Data", required = true) @PathVariable String pavadinimas,
//			@Valid @RequestBody final NaujasLaiskas newBook) {
//		return laiskasService.updateLaiskas(pavadinimas, newBook);
//
//	}
//
//	@RequestMapping(path = "/{pavadinimas}", method = RequestMethod.DELETE)
//	@ResponseStatus(HttpStatus.NO_CONTENT)
//	public List<LaiskasKlientui> deleteBook(@PathVariable final String pavadinimas) {
//		laiskasService.deleteLaiskas(pavadinimas);
//		return laiskasService.getLaiskai();
//	}
//
//}
