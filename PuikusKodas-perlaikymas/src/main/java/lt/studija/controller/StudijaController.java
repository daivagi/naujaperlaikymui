package lt.studija.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lt.studija.model.studija.NaujaStudija;
import lt.studija.model.studija.Studija;
import lt.studija.service.StudijaService;

@RestController
@Api(value = "studija")
@RequestMapping(value = "/api/studijos")
public class StudijaController {
	
	private final StudijaService studijaService;

	
	@Autowired
	public StudijaController(StudijaService studijaService) {
		this.studijaService = studijaService;
	}

	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get Studijos", notes = "Returns list of all studijos")
	public List<Studija> getStudijos() {
		return studijaService.getStudijos();
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "Get Studija", notes = "Returns studija by ID")
	public Studija getStudija(@PathVariable Long id) {
		return studijaService.getStudija(id);
	}
	


	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create studija", notes = "Create studija with data")
	@ResponseStatus(HttpStatus.CREATED)
	public void createStudija(
			@ApiParam(value = "Studija", required = true) @Valid @RequestBody final NaujaStudija naujaStudija) {
		studijaService.createStudija(naujaStudija);

	}

	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	@ApiOperation(value = "Update Studija", notes = "Update Studija with data")
	public Studija updateStudija(@ApiParam(value = "Studija Data", required = true) @PathVariable Long id,
			@Valid @RequestBody final NaujaStudija naujaStudija) {
		return studijaService.updateStudija(id, naujaStudija);

	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Studija", notes = "Delete Studija with data")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteStudija(@PathVariable final Long id) {
		 studijaService.deleteStudija(id);
	}
		
}
		
		

